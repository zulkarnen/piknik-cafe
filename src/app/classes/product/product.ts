import { Category } from "src/app/enums/category";

export class Product {
  public productID:string;
  public title:string;
  public description:string;
  public price:number;
  public category:Category;
  public isReady:boolean;
  public isSuprise: boolean;
  public src: string;
  public created: string;
  public updated: string;


  constructor(title:string='', desc:string='', price:number=0, category:Category, isReady:boolean=true, isSuprise: boolean = false, src:string=' ') {
    let now = new Date().toLocaleString('in-ID')
    this.productID = title.replace(/\s/g, '-').toLowerCase();
    this.title = title;
    this.description = desc;
    this.price = price;
    this.category = category;
    this.src = src;
    this.isReady = isReady;
    this.isSuprise  = isSuprise;
    this.updated = now;
    this.created = now;
  }


}
