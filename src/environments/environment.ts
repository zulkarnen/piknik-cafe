// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebase: {
    projectId: 'piknik-cafe',
    appId: '1:364485643795:web:096cd4837ad4ee6227b9e6',
    storageBucket: 'piknik-cafe.appspot.com',
    locationId: 'asia-southeast2',
    apiKey: 'AIzaSyAx5P6WbOkQjtiokd-qn2MeBI8W-BMGdmU',
    authDomain: 'piknik-cafe.firebaseapp.com',
    messagingSenderId: '364485643795',
  },
  production: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
