export enum Category {
  COFFE = 'Coffee',
  ANY_DRINK = 'Any Drink',
  MILK = 'Milk',
  TEA = 'Tea',
  FOOD = 'Food',
  DESERT = 'Desert'

}
