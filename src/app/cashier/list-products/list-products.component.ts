import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Product } from 'src/app/classes/product/product';
import { ProductsComponent } from 'src/app/products/products.component';
import { ProductsService } from 'src/app/services/products.service';

@Component({
  selector: 'app-list-products',
  templateUrl: './list-products.component.html',
  styleUrls: ['./list-products.component.scss']
})
export class ListProductsComponent implements OnInit {

  products?: Product[];
  checked = false;

  productServices: ProductsService;


  constructor(productServices: ProductsService) {
    this.productServices = productServices;

   }

  ngOnInit(): void {

    this.productServices.getProducts().valueChanges().subscribe( res =>  {

      this.products = res;
      console.log(this.products)
    })

  }

  async deleteProduct(product: Product) {

    if (confirm('Yakin Hapus ?')) {

      try {
        await this.productServices.deteleProductById(product.productID);
        alert(`${product.title} Berhasil Dihapus !`)
      } catch(e) {
        alert(e)
      }

    }

  }



  /**
   * to update isReady prop, if isReady == true maka ubah jadi false dan sebaliknya.
   * @param product
   */
  changeAvailability(product: Product) {

    this.productServices.updateProduct(product.productID, {isReady : !product.isReady})

  }





}
