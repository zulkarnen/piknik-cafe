export class Customer {

  customerID: string;
  displayName: string;
  name: string;
  tableNumber: number;
  isOnline: boolean;
  created: string;
  updated:string;


  constructor(displayName:string, tableNumber:number) {
    let now = new Date().toLocaleString('in-ID')

    let time = new Date().getTime();

    this.customerID = `${time}-${displayName.toUpperCase()}`;
    this.displayName = displayName;
    this.name = displayName.toLowerCase();
    this.tableNumber = tableNumber;
    this.isOnline = true;
    this.updated = now;
    this.created = now;

  }

}
