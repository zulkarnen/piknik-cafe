import { object } from "rxfire/database";
import { State } from "src/app/enums/state";
import { Customer } from "../customer/customer";
import { Product } from "../product/product";

export class Order {

  orderID: string;
  customer?: Customer;
  products: Product[];
  state: State;
  created: string;
  updated: string;


  constructor(customer: Customer, products: Product[]) {
    let now = new Date().toLocaleString('in-ID')

    this.customer = Object.assign({}, customer)
    this.orderID = `ORD-${customer.customerID}`;
    this.products = products;
    this.state = State.RECEIVED;
    this.created = now;
    this.updated = now;

  }



}
