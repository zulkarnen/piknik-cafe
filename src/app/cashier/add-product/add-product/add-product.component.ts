import { Component, OnInit } from '@angular/core';
import { Router, RouterLink } from '@angular/router';
import { async } from '@firebase/util';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { Product } from 'src/app/classes/product/product';
import { Category } from 'src/app/enums/category';
import { ProductsService } from 'src/app/services/products.service';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss']
})

export class AddProductComponent implements OnInit {

  public percent?: Observable<number | undefined>;
  storage = this.productService.storage;

  filePath:string = '';

  public data:any = {};
  public categories;
  public URL;

  constructor(private productService: ProductsService, private router: Router) {
    this.categories = Category
    this.URL = new Observable<string>()
  }

  ngOnInit(): void {
  }


   /**
    * create object Product using input data in /add-product
    * */
   async submitProduct() {
    let product = new Product(this.data.title, this.data.desc, this.data.price, this.data.category, Boolean(this.data.isReady), Boolean(this.data.isSuprise), this.data.src ? this.data.src :" ");
    console.warn(product)

      if (await this.productService.addProduct(product) instanceof Error) {
        alert("GAGAL ! ")
      } else {
        alert('BERHASIl !')
        await this.clearForm()
      }

  }

  /**
   * uploading image to server and observe loading get URL download
   * @param event
   */

  uploadImage(event: any) {


    const file = event.target.files[0]
    const fileName = file.name.replace(/\s/g, '-').toLowerCase();
    const filePath = `test/${fileName}`
    this.filePath = filePath
    // this.filePath = file.name
    const fileRef = this.storage.ref(filePath);
    const task = this.storage.upload(filePath, file);

    this.percent = task.percentageChanges();

    console.info(this.percent?.subscribe(res => res))

    task.snapshotChanges().pipe(
      finalize( () => {
        this.URL = fileRef.getDownloadURL();
        this.URL.subscribe(res => this.data.src = res)
      })
    ).subscribe()

  }



  clearForm() {
    this.data = {};
  }






















  cancleUpload() {

    return new Promise((res, rej) => {

      this.storage.ref(this.filePath).delete()
      .subscribe(r => {
        res("Data di Server sudah di hapus")
      }
      , e => rej(new Error(e)))
    })

  }



  cancleAddProduct() {

    alert(this.data.src != undefined)
    alert(this.data.src)
    if (this.data.src != undefined) {
      if (confirm('Yakin akan cancle ini, ada gambar yang akan dihapus ?')) {
        this.cancleUpload()
        .then(res => {
          alert(res);
          this.clearForm();
          this.router.navigate(['/cashier'])
        })
        .catch(e => alert(e))
      }

    } else {
      this.clearForm();
      this.router.navigate(['/cashier'])
    }

  }




}
