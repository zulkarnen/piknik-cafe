import { Component, OnInit } from '@angular/core';
import { Product } from '../classes/product/product';
import { Category } from '../enums/category';
import { ProductsService } from '../services/products.service';


@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})

export class ProductsComponent implements OnInit {

  products?: Product[];
  productService: ProductsService;
  isOnline = true;


  // Testing
  data: Product;




  constructor(productService: ProductsService) {

   this.productService = productService;

  //  console.log(navigator.onLine)



   // test create object
   this.data = new Product("Mocha Chi", "Asli Indonesia", 1000232, Category.ANY_DRINK)

   console.log(this.data.productID)



  }



  getProducts() {
    this.productService.getProductsIsReady().valueChanges().subscribe(response => {

      console.log(response)

      console.log(this.products)


      try {

        if (response.length !== 0) {
          this.products = response;
        }

        console.log(this.products)

        if (response.length === 0) {

          console.log(this.products)

          this.products = this.products

          console.log(this.products)


          if (this.products?.length === 0 ) {

            throw new RangeError('PRODUCT 0 ')

          } else if (response.length === 0 && this.products === undefined) {
            throw new Error('INTERNET ANDA MATI BOS')

          }
        }


      } catch (e) {

        if (e instanceof RangeError) {
          alert(e)
        } else if (e instanceof Error) {
          this.isOnline = false;
          alert(e)
        }

    }

  })}






   ngOnInit(): void {

    this.getProducts()
   }



  }






// this.service.getProducts()
    // .valueChanges()
    // .subscribe(async (res) => {

    //   if (res.length != 0) {
    //     this.products = res;
    //   } else {
    //     this.isOnline = false
    //     throw new Error('Tidak bisa mendapatkan data produk dan pastikan Anda Terhubung ke internet')
    //   }
    // }, error => {
    //   this.isOnline = false;
    //   console.log(error)
    // });







    // this.service.initest().then((resolve: any) => console.log(resolve))


    // this.onlineEvent = fromEvent(window, 'online');
    // this.offlineEvent = fromEvent(window, 'offline');
    //     this.subscriptions.push(this.onlineEvent.subscribe(event => {
    //         this.connectionStatusMessage = 'Connected to internet! You are online';
    //         this.connectionStatus = 'online';
    //         console.log(event)
    //     }));
    //     this.subscriptions.push(this.offlineEvent.subscribe(e => {
    //         this.connectionStatusMessage = 'Connection lost! You are offline';
    //         this.connectionStatus = 'offline';
    //         console.log(e)
    //     }));

    // load data products
    // try {
    //   this.service.getProducts().then()
    //   .valueChanges({idField: 'test'})
    //   .subscribe(
    //     (res) => {this.products = res},
    //    err => alert('Ini erro dong ' + err))
    // } catch(e) {
    //   console.log(`ini errror`,)
    // }

    // this.st.status.subscribe((status: OnlineStatusType) => alert(status) )

    // if(this.status) {
    //   alert('OFFLINE')
    // } else {
    //   alert('ONLINE')
    // }





