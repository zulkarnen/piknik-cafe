export class Cashier {

  cahierID: string;
  name: string;
  phoneNumber: string;
  address: string;
  hireDate: string;
  updated: string;

  constructor(name: string, phoneNumber: string, hireDate: string, address: string) {

    let nowDate = new Date().toLocaleString('in-ID')

    this.cahierID = ''
    this.name = name;
    this.phoneNumber = phoneNumber;
    this.address = address;
    this.hireDate = hireDate;
    this.updated = nowDate

  }

}
