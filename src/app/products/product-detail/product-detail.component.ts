
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

import { Customer } from 'src/app/classes/customer/customer';
import { Order } from 'src/app/classes/order/order';
import { Product } from 'src/app/classes/product/product';
import { OrderService } from 'src/app/services/order.service';
import { ProductsService } from 'src/app/services/products.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {

  public id: string = ''; // path /id
  product?: Product;
  private productService: ProductsService;
  qty: number = 1;
  public testDataOrder: any;


  constructor(private route: ActivatedRoute, productService: ProductsService, private orderService: OrderService) {
    this.productService = productService;
    this.route.params.subscribe(res => {
      this.id = res.id
    })
    console.error(this.id)
  }

  ngOnInit(): void {

    this.productService.getProductById(this.id).valueChanges().subscribe(res =>  this.product = res)


  }

  orderNow() {
    let quantity = {qty: this.qty}
    let prod:any = Object.assign(this.product, quantity);
    console.log('PROD : ' + prod)


    let testCust: Customer = new Customer('zul', 10) // test jika ada customer
    let orderData: Order = new Order(testCust, prod) // test jika ada order

    this.testDataOrder = orderData



    this.orderService.setOrder(orderData)
  }



}
