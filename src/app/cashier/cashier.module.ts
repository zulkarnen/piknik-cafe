import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CashierComponent } from './cashier/cashier.component';
import { RouterModule, Routes} from '@angular/router';
import { MaterialDesign } from '../material-design/material';
import { ListProductsComponent } from './list-products/list-products.component';
import { AddProductComponent } from './add-product/add-product/add-product.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { FormsModule } from '@angular/forms';
import { Product } from '../classes/product/product';
import { Category } from '../enums/category';
import { AppModule } from '../app.module';

const routes: Routes =  [
  {
    path: '',
    component: CashierComponent,
    children : [
      {
        path: '',
        component: DashboardComponent
      },
      {
        path: 'list-products',
        component: ListProductsComponent
      },
      {
        path: 'add-product',
        component: AddProductComponent
      }
    ]
  },
  {
    path:'cashier',
    component: CashierComponent
  },
  {
    path:'list',
    component:ListProductsComponent
  },
  {
    path: 'add-product',
    component: AddProductComponent
  }
]

@NgModule({
  declarations: [
    CashierComponent,
    ListProductsComponent,
    AddProductComponent,
    DashboardComponent,


  ],
  imports: [
    CommonModule,
    RouterModule,
    MaterialDesign,
    RouterModule.forChild(routes),
    FormsModule

  ]
})
export class CashierModule { }
