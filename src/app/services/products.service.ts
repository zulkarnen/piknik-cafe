import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/compat/firestore'
import { Product } from '../classes/product/product';
import {AngularFireStorage} from '@angular/fire/compat/storage'
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators'
import { where } from 'firebase/firestore';



@Injectable({
  providedIn: 'root'
})

export class ProductsService {

  public firestore: AngularFirestore;
  public storage: AngularFireStorage;

  public uploadPers: Observable<any>;
  downloadURL: Observable<string>;

  public URL?: Observable<string>;
  public percent?: Observable<number | undefined>

  constructor(firestore: AngularFirestore, storage: AngularFireStorage) {

    this.firestore = firestore;
    this.storage = storage;
    this.uploadPers = new Observable<any>();
    this.downloadURL = new Observable<string>();


   }


   /**
    * To get product only atribut isReady == true. For digunakan di halaman product user view.
    * @returns
    */
   getProductsIsReady(): AngularFirestoreCollection<Product> {
     return this.firestore.collection<Product>('products', ref => ref.where('isReady', '==', true));
   }


   /**
    * function to get all products untuk digunakan user Cashier.
    * @returns AngularFirestoreCollection to observe in componenet.
    */
   getProducts() :  AngularFirestoreCollection<Product> {
    return this.firestore.collection<Product>('products');
   }



   getProductById(id: string) {
     return this.firestore.collection<Product>('products').doc(id);
   }


   /**
    * checking duplicate product in server using id of product
    * @param id from product yang akan ditambahkan.
    * @returns Promise<Boolean> resolve true or false, reject E from observer.
    */
   checkDuplicate(id: any) {
    return new Promise((duplicated, reject) => {
      this.firestore.collection('products', ref => ref.where('productID', '==', id))
      .valueChanges()
      .subscribe(res => {
        console.info(res.length)
        if (res.length !== 0) {
         duplicated(true)
       } else {
         duplicated(false)
       }
       }, error => reject(error))
    })
  }


  /**
   * add one product to server
   * @param data
   * @returns true if no duplicate else throw Error
   */
  async addProduct(data: Product) {


    if (await this.checkDuplicate(data.productID)) {
      alert('DUPLICATE BANGET')
      return new Error('Duplicate DUPLICATE DUPLICATE')

    }

      // let now = new Date().toLocaleString('in-ID')
      // let object:Product =  { created:now, updated:now , ...data}
      await this.firestore.collection('products')
      .doc(data.productID).set(Object.assign({}, data))
      .then(r => alert('BERHASIL SERVICE'))
      .catch(e => alert('TIDAK BERHASIL SERVICE')); // test
      return true

    }

    /**
     * function for update data of one product, dan akan mengupdate prop updated menjadi waktu sekarang.
     * @param id id of product
     * @param newData props and values yang di update
     */
    updateProduct(id:string, newData:any) {
      let now = new Date().toLocaleString('in-ID')
      console.info(newData)
      const data = Object.assign({updated: now}, newData)
      console.error(data);
      this.firestore.collection('products').doc(id).update(data) // test
    }


    /**
     * delete one product
     * @param id
     * @returns Promise
     */
    deteleProductById(id: string) {
      return this.firestore.collection('products').doc(id).delete() // test
    }


    /**
     * Function test tidak bisa Emit number of loading ke component.
     * @param event
     */
    uploadImage(event: any) {

      const file = event.target.files[0]
      console.log(file)
      const filePath = `test/${file.name}`
      const fileRef = this.storage.ref(filePath);
      const task = this.storage.upload(filePath, file);
      this.percent = task.percentageChanges();
      console.info(this.percent.subscribe(res => res))
      task.snapshotChanges().pipe(
        finalize( () => {
          this.URL = fileRef.getDownloadURL();
        })
      ).subscribe()


    }

}
