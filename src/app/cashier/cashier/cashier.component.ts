import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/classes/product/product';
import { Category } from 'src/app/enums/category';
import { ProductsService } from 'src/app/services/products.service';

@Component({
  selector: 'app-cashier',
  templateUrl: './cashier.component.html',
  styleUrls: ['./cashier.component.scss']
})
export class CashierComponent implements OnInit {

  service: ProductsService;
  categories;

  constructor(service: ProductsService) {

    this.service = service;
    this.categories = Category;

   }

  ngOnInit(): void {

  }



  // Testing upload and update

  testUpload() {
    const data = new Product('Test', 'Ini adalah Test', 100, Category.ANY_DRINK);

    this.service.addProduct(data)

  }


  update() {

    // this.service.updateProduct()

  }

}
